export interface IVerificationTokenGenerator {
  generateToken(): Promise<string>;
}

export interface IPasswordResetTokenGenerator {
  generateToken(): Promise<string>;
}

export interface ISaltGenerator {
  generateSalt(): Promise<string>;
}

export interface IHasher {
  hash(input: string): Promise<string>;
  compareHash(input: string, actualHash: string): Promise<boolean>;
}

export interface IPbkdf2Config {
  iterations: number;
  outputBytes: number;
  hashAlgorithm: string;
}
