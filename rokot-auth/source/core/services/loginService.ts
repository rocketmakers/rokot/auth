import * as Logger from "bunyan";
import { ILoginRequest, IUser } from "../core";
import { ILoginService } from "../services";
import { IUserRepository, IDataContext } from "../data";
import { VersioningHasher } from "../../security/versioningHasher";

export class LoginService<TId extends string = string> implements ILoginService<TId> {
  constructor(
    private userRepository: IUserRepository<TId>,
    private hasher: VersioningHasher,
    private logger: Logger
  ) { }

  async login({ username, password }: ILoginRequest, dataContext?: IDataContext): Promise<IUser<TId> | null> {
    const user = await this.userRepository.getUserByUsername(username, dataContext);
    if (!user) {
      this.logger.trace(`Login failed: no such user '${username}'`);
      return null;
    }

    const actualPasswordHash = await this.userRepository.getPasswordHashByUsername(username, dataContext);
    const match = await this.hasher.compareHash(password, actualPasswordHash);
    if (!match) {
      this.logger.trace(`Login failed: password doesn't match for user '${username}'`);
      return null;
    }

    this.logger.trace(`Login successful for user '${username}'`);

    const latestHasherVersion = this.hasher.getLatestHasherVersion();
    if (latestHasherVersion !== actualPasswordHash.version) {
      const newHash = await this.hasher.hash(password);
      await this.userRepository.setUserPassword(username, newHash, dataContext);
      this.logger.trace(`Updated password hash for user '${username}' to version ${latestHasherVersion}`);
    }

    return user;
  }
}
