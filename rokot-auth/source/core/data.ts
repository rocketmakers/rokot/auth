import { IUser, INewUser, IVersionedHash } from "./core";

/**
 * Used to maintain a context across repository calls. Useful for transactions.
 */
export interface IDataContext { }

/**
 * Used to persist username/password login information.
 */
export interface IUserRepository<TId extends string = string> {
  /**
   * Return true if a user with the given username exists.
   */
  userExists(username: string, context?: IDataContext): Promise<boolean>;
  /**
   * Gets the user with the given username, or null if no such user exists.
   */
  getUserByUsername(username: string, context?: IDataContext): Promise<IUser<TId> | null>;
  /**
   * Gets the versioned password hash for the user with the given username. The
   * user is guaranteed to exist at this point.
   */
  getPasswordHashByUsername(username: string, context ?: IDataContext): Promise<IVersionedHash>;
  /**
   * Creates a new user. Don't set a password hash for this user.
   */
  createUser(newUser: INewUser, context ?: IDataContext): Promise<void>;
  /**
   * Sets the password for the user with the given username. Be sure to persist
   * the version along with the hash, as we need it to compare with the password
   * given on login attempts.
   */
  setUserPassword(username: string, passwordHash: IVersionedHash, context ?: IDataContext): Promise<void>;
  }

/**
 * Used to persist verification and password reset tokens.
 */
export interface ITokenRepository {
  /**
   * Get all valid verification tokens for the user with the given username. If
   * you need to support expiry or revocation of tokens, do it in this method.
   */
  getVerificationTokenHashes(username: string, context?: IDataContext): Promise<IVersionedHash[]>;
  /**
   * Persist a verification token for the user with the given username. Be sure
   * to persist the version along with the hash, as we'll need it later.
   */
  createVerificationTokenHash(username: string, verificationTokenHash: IVersionedHash, context?: IDataContext): Promise<void>;
  /**
   * Delete all verification tokens for the user with the given username.
   */
  destroyAllVerificationTokenHashes(username: string, context?: IDataContext): Promise<void>;
  /**
   * Get all valid password reset tokens for the user with the given username.
   * If you need to support expiry or revocation of tokens, do it in this
   * method.
   */
  getPasswordResetTokenHashes(username: string, context?: IDataContext): Promise<IVersionedHash[]>;
  /**
   * Persist a password reset token for the user with the given username. Be
   * sure to persist the version along with the hash, as we'll need it later.
   */
  createPasswordResetTokenHash(username: string, passwordResetTokenHash: IVersionedHash, context?: IDataContext): Promise<void>;
  /**
   * Delete all password reset tokens for the user with the given username.
   */
  destroyAllPasswordResetTokenHashes(username: string, context?: IDataContext): Promise<void>;
}
