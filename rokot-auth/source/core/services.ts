import {
  IUser,
  IUserCreationRequest,
  IUserRegistrationRequest,
  IUserCreationResponse,
  IUserRegistrationResponse,
  IVerifyUserRequest,
  IForgotPasswordRequest,
  IForgotPasswordResponse,
  IResetPasswordRequest,
  IChangePasswordRequest,
  ILoginRequest
} from "./core";
import { IDataContext } from "./data";

export interface IUserCreationService<TId extends string = string> {
  /**
   * Create a new user on their behalf. Pass through any additional fields that
   * need to be saved on the newUser object. Returns a verification token that
   * should be sent to the user over the same channel as password reset tokens
   * will be.
   */
  createUser(userCreation: IUserCreationRequest, dataContext?: IDataContext): Promise<IUserCreationResponse<TId>>;

  /**
   * Creates a user verification token
   * This can be used to create a token for an imported (migrated?) user that doesn't have a password
   */
  createUserVerificationToken(username: string, dataContext?: IDataContext): Promise<string>
}

export interface IUserRegistrationService<TId extends string = string> {
  /**
   * Register a new user as the user themselves. Pass through any additional
   * fields that need to be saved on the newUser object. Returns a verification
   * token that should be sent to the user over the same channel as password
   * reset tokens will be.
   */
  registerUser(userRegistration: IUserRegistrationRequest, dataContext?: IDataContext): Promise<IUserRegistrationResponse<TId>>;
}

export interface ILoginService<TId extends string = string> {
  /**
   * Login with a username and password. Returns the user on successful login, or
   * null on failure.
   */
  login(loginRequest: ILoginRequest, dataContext?: IDataContext): Promise<IUser<TId> | null>;
}

export interface IUserVerificationService {
  /**
   * Validates a user's password reset token. Requires the password reset token which was sent
   * to the user as part of the forgot password flow, and the username.
   */
  validateVerificationToken(username: string, verificationToken: string, dataContext?: IDataContext): Promise<void>;
  /**
   * Verify a user's ability to receive messages over a side-channel, and set an
   * initial password. Requires the verification token which was sent to the
   * user as part of user creation.
   */
  verifyUser(verifyUserRequest: IVerifyUserRequest, dataContext?: IDataContext): Promise<void>;
}

export interface IForgotPasswordService {
  /**
   * Generate a password reset token for a user that has forgotten their
   * password. Returns a verification token that should be sent to the user over
   * the same channel as the verification token was.
   */
  forgotPassword(forgotPasswordRequest: IForgotPasswordRequest, dataContext?: IDataContext): Promise<IForgotPasswordResponse>;
}

export interface IResetPasswordService {
  /**
   * Validates a user's password reset token. Requires the password reset token which was sent
   * to the user as part of the forgot password flow, and the username.
   */
  validateResetPasswordToken(username: string, passwordResetToken: string, dataContext?: IDataContext): Promise<void>;
  /**
   * Resets a user's password. Requires the password reset token which was sent
   * to the user as part of the forgot password flow.
   */
  resetPassword(resetPasswordRequest: IResetPasswordRequest, dataContext?: IDataContext): Promise<void>;
}

export interface IChangePasswordService {
  /**
   * Changes a user's password. Requires the existing password.
   * @param  {IChangePasswordRequest} changePasswordRequest [description]
   * @param  {IDataContext}           dataContext           [description]
   * @return {Promise<void>}                                [description]
   */
  changePassword(changePasswordRequest: IChangePasswordRequest, dataContext?: IDataContext): Promise<void>;
}
