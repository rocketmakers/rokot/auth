import { expect, sinon } from "rokot-test";
import * as bunyan from "bunyan";

import { MemoryUserRepository } from "./memoryRepositories/memoryUserRepository";
import { MemoryTokenRepository } from "./memoryRepositories/memoryTokenRepository";

import {
  IUserCreationService,
  makeUserCreationService,
  IUserRegistrationService,
  makeUserRegistrationService,
  IUserVerificationService,
  makeUserVerificationService,
  ILoginService,
  makeLoginService,
  IChangePasswordService,
  makeChangePasswordService,
  IForgotPasswordService,
  makeForgotPasswordService,
  IResetPasswordService,
  makeResetPasswordService
} from "../../index";

describe("rokot-auth @integration", () => {
  let userCreationService: IUserCreationService;
  let userRegistrationService: IUserRegistrationService;
  let userVerificationService: IUserVerificationService;
  let loginService: ILoginService;
  let changePasswordService: IChangePasswordService;
  let forgotPasswordService: IForgotPasswordService;
  let resetPasswordService: IResetPasswordService;

  beforeEach(() => {
    const userRepository = new MemoryUserRepository();
    const tokenRepository = new MemoryTokenRepository();

    userCreationService = makeUserCreationService(userRepository, tokenRepository, bunyan.createLogger({ name: "test" }));
    userRegistrationService = makeUserRegistrationService(userRepository, bunyan.createLogger({ name: "test" }));
    userVerificationService = makeUserVerificationService(userRepository, tokenRepository, bunyan.createLogger({ name: "test" }));
    loginService = makeLoginService(userRepository, bunyan.createLogger({ name: "test" }));
    changePasswordService = makeChangePasswordService(userRepository, tokenRepository, bunyan.createLogger({ name: "test" }));
    forgotPasswordService = makeForgotPasswordService(userRepository, tokenRepository, bunyan.createLogger({ name: "test" }));
    resetPasswordService = makeResetPasswordService(userRepository, tokenRepository, bunyan.createLogger({ name: "test" }));
  });

  it("all works :)", async () => {
    // create bruce, get a verification token
    const { verificationToken } = await userCreationService.createUser({ username: "bruce" });

    // validate verification token for bruce
    await userVerificationService.validateVerificationToken("bruce", verificationToken)
    // verify with the token, set password to "batman"
    await userVerificationService.verifyUser({ username: "bruce", password: "batman", verificationToken });

    // login works with password "batman"
    expect(await loginService.login({ username: "bruce", password: "batman" })).to.exist;
    // login doesn't work with password "superman"
    expect(await loginService.login({ username: "bruce", password: "superman" })).to.be.null;

    // change bruce's password to "gotham"
    await changePasswordService.changePassword({ username: "bruce", oldPassword: "batman", newPassword: "gotham" });

    // login no longer works with password "batman"
    expect(await loginService.login({ username: "bruce", password: "batman" })).to.be.null;
    // login works with password "gotham"
    expect(await loginService.login({ username: "bruce", password: "gotham" })).to.exist;

    // bruce forgot his password, get a reset token
    const { passwordResetToken } = await forgotPasswordService.forgotPassword({ username: "bruce" });
    // verify password reset token for bruce
    await resetPasswordService.validateResetPasswordToken("bruce", passwordResetToken)
    // reset password to "batman" with the token
    await resetPasswordService.resetPassword({ username: "bruce", password: "batman", passwordResetToken });

    // login no longer works with password "gotham"
    expect(await loginService.login({ username: "bruce", password: "gotham" })).to.be.null;
    // login works with password "batman"
    expect(await loginService.login({ username: "bruce", password: "batman" })).to.exist;

    // self-register another user
    await userRegistrationService.registerUser({ username: "dick", password: "nightwing" });
    // login works immediately
    expect(await loginService.login({ username: "dick", password: "nightwing" })).to.exist;
  });
});
