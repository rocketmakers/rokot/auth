import { expect, sinon } from "rokot-test";
import * as bunyan from "bunyan";

import { IUserRepository } from "../../core/data";
import { IHasher } from "../../core/security";
import { UserRegistrationService } from "../../core/services/userRegistrationService";
import { VersioningHasher } from "../../security/versioningHasher";

describe("UserRegistrationService", () => {
  let sut: UserRegistrationService;
  let createUserSpy: sinon.SinonSpy;
  let setUserPasswordSpy: sinon.SinonSpy;

  beforeEach(() => {
    const userExistsStub = sinon.stub();
    userExistsStub.withArgs("existing@user.com").returns(Promise.resolve(true));
    userExistsStub.returns(Promise.resolve(false));

    const getUserByUsernameStub = sinon.stub();
    getUserByUsernameStub.withArgs("new@user.com").returns({ id: 2, username: "new@user.com" });
    getUserByUsernameStub.returns(null);

    createUserSpy = sinon.spy();
    setUserPasswordSpy = sinon.spy();

    const userRepository: IUserRepository = <any>{
      userExists: userExistsStub,
      getUserByUsername: getUserByUsernameStub,
      createUser: createUserSpy,
      setUserPassword: setUserPasswordSpy
    };

    const hasher: IHasher = <any>{
      hash: sinon.stub().withArgs("password").returns("hashed password")
    };

    const vHasher = new VersioningHasher();
    vHasher.register(1, hasher);

    sut = new UserRegistrationService(userRepository, vHasher, bunyan.createLogger({ name: "test" }));
  });

  describe("#registerUser", () => {
    it("returns new user on success", async () => {
      const result = await sut.registerUser({ username: "new@user.com", password: "password" });
      expect(result.user.id).to.equal(2);
      expect(result.user.username).to.equal("new@user.com");
    });

    it("calls createUser on repository", async () => {
      await sut.registerUser({ username: "new@user.com", password: "password" });
      expect(createUserSpy.calledWith({ username: "new@user.com" })).to.be.true;
    });

    it("calls setUserPassword on repository", async () => {
      await sut.registerUser({ username: "new@user.com", password: "password" });
      expect(createUserSpy.calledWith({ username: "new@user.com" })).to.be.true;
    });

    it("throws when user already exists", async () => {
      await expect(sut.registerUser({ username: "existing@user.com", password: "password" })).to.be.rejected;
    });
  });
});
