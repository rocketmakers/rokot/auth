import { expect, sinon } from "rokot-test";
import * as bunyan from "bunyan";

import { IUserRepository, ITokenRepository } from "../../core/data";
import { IHasher, IPasswordResetTokenGenerator } from "../../core/security";
import { ForgotPasswordService } from "../../core/services/forgotPasswordService";
import { VersioningHasher } from "../../security/versioningHasher";

describe("ForgotPasswordService", () => {
  let sut: ForgotPasswordService;
  let createPasswordResetTokenHashSpy: sinon.SinonSpy;

  beforeEach(() => {
    const userExistsStub = sinon.stub();
    userExistsStub.withArgs("existing@user.com").returns(true);
    userExistsStub.returns(false);

    const userRepository: IUserRepository = <any>{
      userExists: userExistsStub
    };

    createPasswordResetTokenHashSpy = sinon.spy();

    const tokenRepository: ITokenRepository = <any>{
      createPasswordResetTokenHash: createPasswordResetTokenHashSpy
    };

    const hasher: IHasher = <any>{
      hash: sinon.stub().withArgs("token").returns("hashed token")
    };

    const vHasher = new VersioningHasher();
    vHasher.register(1, hasher);

    const tokenGenerator: IPasswordResetTokenGenerator = {
      generateToken: sinon.stub().returns("token")
    };

    sut = new ForgotPasswordService(userRepository, tokenRepository, tokenGenerator, vHasher, bunyan.createLogger({ name: "test" }));
  });

  describe("#forgotPassword", () => {
    it("returns user and password reset token on success", async () => {
      const result = await sut.forgotPassword({ username: "existing@user.com" });
      expect(result.username).to.equal("existing@user.com");
      expect(result.passwordResetToken).to.equal("token")
    });

    it("calls createPasswordResetToken on repository", async () => {
      await sut.forgotPassword({ username: "existing@user.com" });
      expect(createPasswordResetTokenHashSpy.calledWith("existing@user.com", { version: 1, hash: "hashed token" })).to.be.true;
    });

    it("throws when user does not exist", async () => {
      await expect(sut.forgotPassword({ username: "nota@user.com" })).to.be.rejected;
    });
  });
});
