import { expect, sinon } from "rokot-test";
import * as bunyan from "bunyan";

import { IUserRepository, ITokenRepository } from "../../core/data";
import { IHasher, IVerificationTokenGenerator } from "../../core/security";
import { UserCreationService } from "../../core/services/userCreationService";
import { VersioningHasher } from "../../security/versioningHasher";

describe("UserCreationService", () => {
  let sut: UserCreationService;
  let createUserSpy: sinon.SinonSpy;
  let createVerificationTokenHashSpy: sinon.SinonSpy;

  beforeEach(() => {
    const userExistsStub = sinon.stub();
    userExistsStub.withArgs("existing@user.com").returns(Promise.resolve(true));
    userExistsStub.returns(Promise.resolve(false));

    const getUserByUsernameStub = sinon.stub();
    getUserByUsernameStub.withArgs("new@user.com").returns({ id: 2, username: "new@user.com" });
    getUserByUsernameStub.returns(null);

    createUserSpy = sinon.spy();

    const userRepository: IUserRepository = <any>{
      userExists: userExistsStub,
      getUserByUsername: getUserByUsernameStub,
      createUser: createUserSpy
    };

    createVerificationTokenHashSpy = sinon.spy();

    const tokenRepository: ITokenRepository = <any>{
      createVerificationTokenHash: createVerificationTokenHashSpy
    };

    const tokenGenerator: IVerificationTokenGenerator = {
      generateToken: sinon.stub().returns("token")
    };

    const hasher: IHasher = <any>{
      hash: sinon.stub().withArgs("token").returns("hashed token")
    };

    const vHasher = new VersioningHasher();
    vHasher.register(1, hasher);

    sut = new UserCreationService(userRepository, tokenRepository, tokenGenerator, vHasher, bunyan.createLogger({ name: "test" }));
  });

  describe("#createUser", () => {
    it("returns new user and verification token on success", async () => {
      const result = await sut.createUser({ username: "new@user.com" });
      expect(result.user.id).to.equal(2);
      expect(result.user.username).to.equal("new@user.com");
      expect(result.verificationToken).to.equal("token");
    });

    it("calls createUser on repository", async () => {
      await sut.createUser({ username: "new@user.com" });
      expect(createUserSpy.calledWith({ username: "new@user.com" })).to.be.true;
    });

    it("calls createVerificationToken on repository", async () => {
      await sut.createUser({ username: "new@user.com" });
      expect(createVerificationTokenHashSpy.calledWith("new@user.com", { version: 1, hash: "hashed token" })).to.be.true;
    });

    it("throws when user already exists", async () => {
      await expect(sut.createUser({ username: "existing@user.com" })).to.be.rejected;
    });
  });
});
