import { expect, sinon } from "rokot-test";
import * as bunyan from "bunyan";

import { IUserRepository, ITokenRepository } from "../../core/data";
import { IHasher } from "../../core/security";
import { ChangePasswordService } from "../../core/services/changePasswordService";
import { VersioningHasher } from "../../security/versioningHasher";

describe("ChangePasswordService", () => {
  let sut: ChangePasswordService;
  let setUserPasswordSpy: sinon.SinonSpy;
  let destroyAllPasswordResetTokenHashesSpy: sinon.SinonSpy;

  beforeEach(() => {
    const userExistsStub = sinon.stub();
    userExistsStub.withArgs("existing@user.com").returns(Promise.resolve(true));
    userExistsStub.withArgs("notoken@user.com").returns(Promise.resolve(true));
    userExistsStub.returns(Promise.resolve(false));

    setUserPasswordSpy = sinon.spy();

    const getPasswordHashByUsernameStub = sinon.stub();
    getPasswordHashByUsernameStub.withArgs("existing@user.com").returns({ version: 1, hash: "hashed old password" });
    getPasswordHashByUsernameStub.returns(null);

    const userRepository: IUserRepository = <any>{
      userExists: userExistsStub,
      getPasswordHashByUsername: getPasswordHashByUsernameStub,
      setUserPassword: setUserPasswordSpy
    };

    destroyAllPasswordResetTokenHashesSpy = sinon.spy();

    const tokenRepository: ITokenRepository = <any>{
      destroyAllPasswordResetTokenHashes: destroyAllPasswordResetTokenHashesSpy
    };

    const compareHashStub = sinon.stub();
    compareHashStub.withArgs("old password", "hashed old password").returns(Promise.resolve(true));
    compareHashStub.returns(Promise.resolve(false));

    const hasher: IHasher = {
      hash: sinon.stub().withArgs("new password").returns("hashed new password"),
      compareHash: compareHashStub
    };

    const vHasher = new VersioningHasher();
    vHasher.register(1, hasher);

    sut = new ChangePasswordService(userRepository, tokenRepository, vHasher, bunyan.createLogger({ name: "test" }));
  });

  describe("#changePassword", () => {
    it("calls setUserPassword on repository", async () => {
      await sut.changePassword({ username: "existing@user.com", oldPassword: "old password", newPassword: "new password" });
      expect(setUserPasswordSpy.calledWith("existing@user.com", { version: 1, hash: "hashed new password" })).to.be.true;
    });

    it("calls destroyPasswordResetToken on repository", async () => {
      await sut.changePassword({ username: "existing@user.com", oldPassword: "old password", newPassword: "new password" });
      expect(destroyAllPasswordResetTokenHashesSpy.calledWith("existing@user.com")).to.be.true;
    });

    it("throws when user doesn't exist", async () => {
      await expect(sut.changePassword({ username: "new@user.com", oldPassword: "old password", newPassword: "new password" })).to.be.rejected;
    });

    it("throws when old password is wrong", async () => {
      await expect(sut.changePassword({ username: "existing@user.com", oldPassword: "fake password", newPassword: "new password" })).to.be.rejected;
    });
  });
});
