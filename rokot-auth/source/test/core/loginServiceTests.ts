import { expect, sinon } from "rokot-test";
import * as bunyan from "bunyan";

import { IUserRepository } from "../../core/data";
import { IHasher } from "../../core/security";
import { LoginService } from "../../core/services/loginService";
import { VersioningHasher } from "../../security/versioningHasher";

describe("LoginService", () => {
  let sut: LoginService;

  beforeEach(() => {
    const getUserByUsernameStub = sinon.stub();
    getUserByUsernameStub.withArgs("existing@user.com").returns({ id: 1, username: "existing@user.com" });
    getUserByUsernameStub.returns(null);

    const getPasswordHashByUsernameStub = sinon.stub();
    getPasswordHashByUsernameStub.withArgs("existing@user.com").returns({ version: 1, hash: "hashed password" });
    getPasswordHashByUsernameStub.returns(null);

    const userRepository: IUserRepository = <any>{
      getUserByUsername: getUserByUsernameStub,
      getPasswordHashByUsername: getPasswordHashByUsernameStub
    };

    const compareHashStub = sinon.stub();
    compareHashStub.withArgs("password", "hashed password").returns(true);
    compareHashStub.returns(false);

    const hasher: IHasher = <any>{
      compareHash: compareHashStub
    };

    const vHasher = new VersioningHasher();
    vHasher.register(1, hasher);

    sut = new LoginService(userRepository, vHasher, bunyan.createLogger({ name: "test" }));
  });

  describe("#login", () => {
    it("returns true when username and password are correct", async () => {
      const user = await sut.login({ username: "existing@user.com", password: "password" });
      expect(user).to.exist;
      expect(user!.id).to.equal(1);
      expect(user!.username).to.equal("existing@user.com");
    });

    it("returns false when user doesn't exist", async () => {
      const user = await sut.login({ username: "nota@user.com", password: "password" });
      expect(user).to.be.null;
    });

    it("returns false when password is wrong", async () => {
      const user = await sut.login({ username: "existing@user.com", password: "not the password" });
      expect(user).to.be.null;
    });
  });
});
