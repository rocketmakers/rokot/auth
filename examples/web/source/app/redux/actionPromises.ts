import { Actions } from "./actions";
import { StoreActionFactory } from "./../../core/redux/storeActionFactory";
import {apiClient,tokenStore} from "../../api";

export class ActionPromises {
  static loginJwt(username: string, password: string) {
    return StoreActionFactory.thunk((d, gs) => {
      apiClient.jwt.loginJwt({ username, password }).then(r => {
        if (r.json && r.json.token) {
          tokenStore.set(r.json.token)
          d<any>(StoreActionFactory.api(Actions.Profile, apiClient.jwt.getProfile()))
        }
      })
    })
  }
  static profileJwt() {
    return StoreActionFactory.api(Actions.Profile, apiClient.jwt.getProfile())
  }

  static login(username: string, password: string) {
    return StoreActionFactory.thunk((d, gs) => {
      apiClient.authentication.login({ username, password }).then(r => {
        if (r.response.status === 204) {
          d<any>(StoreActionFactory.api(Actions.Profile, apiClient.authentication.getProfile()))
        }
      })
    })
  }
  static profile() {
    return StoreActionFactory.api(Actions.Profile, apiClient.authentication.getProfile())
  }
  static logout() {
    return StoreActionFactory.thunk((d, gs) => {
      tokenStore.set(undefined)
      apiClient.authentication.logout().then(r => {
        if (r.response.status === 204) {
          d<any>(StoreActionFactory.action(Actions.Logout, undefined))
        }
      })
    })
  }
}
