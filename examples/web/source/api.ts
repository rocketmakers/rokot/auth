import {Qs,IFetcher,ApiClient} from "./clientApi";
import { Fetcher } from "./core/api/fetcher";

export class TokenStore{
  private token: string
  constructor(){
    this.token = localStorage.getItem("jwt")
    console.log("TOKEN", this.token)
  }

  set(token: string){
    localStorage.setItem("jwt", token)
    this.token = token;
  }
  get(){
    return this.token;
  }
}
export const tokenStore = new TokenStore()

class ApiClientFetcher implements IFetcher {
  request<T>(url: string, verb: string, contentType: string, body?: any): Promise<IFetchApiResponse<T>>{
    if (contentType === "application/json") {
      return Fetcher.sendJson(verb, `http://localhost:3000${url}`, body, tokenStore.get())
    }
  }
  buildQueryString(query?: any): string{
    return Qs.buildQueryString(query)
  }
}

export const apiClient = new ApiClient(new ApiClientFetcher())
