interface INewUserRequest {
  username: string;
  displayName: string;
}
interface IUser {
    id: string;
    username: string;
}
interface INewUserResponse {
    user: IUser;
    verificationToken: string;
}
interface IVerifyUserRequest {
    username: string;
    password: string;
    verificationToken: string;
}
interface IForgotPasswordRequest {
    username: string;
}
interface IForgotPasswordResponse {
    username: string;
    passwordResetToken: string;
}
interface IResetPasswordRequest {
    username: string;
    password: string;
    passwordResetToken: string;
}
interface IChangePasswordRequest {
    username: string;
    oldPassword: string;
    newPassword: string;
}
interface ILoginRequest {
    username: string;
    password: string;
}
interface IProfile {
  id: number;
  displayName: string;
}
