import { CustomErrors } from "./../api/errors"
import * as Redux from "redux";
function actionFactory<T>(action: IStoreAction<any>, status: StateStatus, payload: T): IStoreAction<T> {
  return { type: action.type, status, payload, context: action.context }
}

export const apiPromise: Redux.Middleware = store => next => action => {
  if (!action.apiPromise) {
    return next(action)
  }
  store.dispatch(actionFactory(action, StateStatus.Pending, action.pendingPayload));
  return action.apiPromise.then((r: IFetchApiResponse<any>) => {
    store.dispatch(actionFactory(action, StateStatus.Ok, action.okPayload || r.json))
    return r;
  }).catch((er: Error) => {
    store.dispatch(actionFactory(action, StateStatus.Error, er));
    return Promise.reject(er);
  })
}

export const promise: Redux.Middleware = store => next => action => {
  if (!action.promise) {
    return next(action)
  }

  store.dispatch(actionFactory(action, StateStatus.Pending, action.pendingPayload));
  return action.promise.then((r: any) => {
    store.dispatch(actionFactory(action, StateStatus.Ok, action.okPayload || r))
    return r;
  }).catch((er) => {
    store.dispatch(actionFactory(action, StateStatus.Error, er));
    return Promise.reject(er);
  })
};

export const thunkMiddleware: Redux.Middleware = store => next => action => {
  if (!action.thunk) {
    return next(action);
  }
  return action.thunk(store.dispatch, store.getState);
}

export const middlewares = [thunkMiddleware, apiPromise, promise];
