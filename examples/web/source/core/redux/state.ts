export class StateFragment<T> implements IStateFragment<T>{

  constructor(public data: T, public status = StateStatus.NotSet, public context?: any) {
  }
}

export function stateFragmentFactory<T>(state: StateFragment<T>, action: IStoreAction<any>, payloadModifier?: (payload: any) => T, onOk?: (newState: StateFragment<T>) => void) {
  const newState = new StateFragment<T>(undefined);
  newState.status = action.status || StateStatus.Ok;
  newState.context = action.context;
  if (newState.status === StateStatus.Pending) {
    if (action.context && state.context && action.context.id === state.context.id) {
      newState.data = state.data;
    }
  }
  else if (newState.status === StateStatus.Ok) {
    newState.data = payloadModifier ? payloadModifier(action.payload) : action.payload;
    if (onOk) {
      onOk(newState);
    }
  }

  return newState;
}
