/// <reference path="./clientApi.d.ts"/>

export interface IFetcher {
  request<T>(url: string, verb: string, contentType: string, body?: any): Promise<IFetchApiResponse<T>>
  buildQueryString?(query?: any): string
}

export class Qs {
  private static buildQueryStringValue(value: any, key: string) {
    return value
  }

  static buildQueryString(query?: any) {
    if (!query) {
      return ""
    }

    const qs = Object.keys(query).map(k => {
      return `${k}=${Qs.buildQueryStringValue(query[k], k)}`
    }).join("&")

    return qs.length ? `?${qs}` : ""
  }
}

function optionalParam(parameter) {
  return parameter ? `/${parameter}` : ""
}


export class AuthenticationController {
  constructor(private fetcher: IFetcher){}

  createUser(body: INewUserRequest): Promise<IFetchApiResponse<INewUserResponse>> {
    return this.fetcher.request<INewUserResponse>(`/auth/user`, "post", "application/json", body)
  }

  verifyUser(body: IVerifyUserRequest): Promise<IFetchApiResponse<void>> {
    return this.fetcher.request<void>(`/auth/verify`, "post", "application/json", body)
  }

  forgotPassword(body: IForgotPasswordRequest): Promise<IFetchApiResponse<IForgotPasswordResponse>> {
    return this.fetcher.request<IForgotPasswordResponse>(`/auth/forgot-password`, "post", "application/json", body)
  }

  resetPassword(body: IResetPasswordRequest): Promise<IFetchApiResponse<void>> {
    return this.fetcher.request<void>(`/auth/reset-password`, "post", "application/json", body)
  }

  changePassword(body: IChangePasswordRequest): Promise<IFetchApiResponse<void>> {
    return this.fetcher.request<void>(`/auth/change-password`, "post", "application/json", body)
  }

  login(body: ILoginRequest): Promise<IFetchApiResponse<void>> {
    return this.fetcher.request<void>(`/auth/login`, "post", "application/json", body)
  }

  logout(): Promise<IFetchApiResponse<void>> {
    return this.fetcher.request<void>(`/auth/logout`, "get", "application/json")
  }

  twitterLogin(): Promise<IFetchApiResponse<void>> {
    return this.fetcher.request<void>(`/auth/twitter`, "get", "application/json")
  }

  twitterCallback(): Promise<IFetchApiResponse<any>> {
    return this.fetcher.request<any>(`/auth/twitter/callback`, "get", "application/json")
  }

  getProfile(): Promise<IFetchApiResponse<IProfile>> {
    return this.fetcher.request<IProfile>(`/auth/profiles/me`, "get", "application/json")
  }
}
export class JwtController {
  constructor(private fetcher: IFetcher){}

  loginJwt(body: ILoginRequest): Promise<IFetchApiResponse<{ token: string }>> {
    return this.fetcher.request<{ token: string }>(`/jwt/login`, "post", "application/json", body)
  }

  getProfile(): Promise<IFetchApiResponse<IProfile>> {
    return this.fetcher.request<IProfile>(`/jwt/profiles/me`, "get", "application/json")
  }
}
export class ApiClient {
  constructor(private fetcher: IFetcher){}

  authentication = new AuthenticationController(this.fetcher)
  
  jwt = new JwtController(this.fetcher)
  
}
//export const apiClient = new ApiClient()
