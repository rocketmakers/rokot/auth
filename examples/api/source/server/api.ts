import * as express from 'express';
import * as _ from 'underscore';
import {PassportAuth} from "./passport";
import {createDefaultRuntimeApi} from "rokot-apicontroller";
import {CustomExpressRouteBuilder} from "./expressRequest";
import {Logger} from "rokot-log";
import * as cors from "cors";
import {kernel} from "../ioc/kernel";

export interface IApiConfig {
  port: number;
  errorRequestHandler?: express.ErrorRequestHandler
}

const errorRequestHandler: express.ErrorRequestHandler = (err, req, res, next) => {
  res.status(500).send(err.message || err)
}

export class Api {
  constructor(private logger: Logger, private config: IApiConfig, private passportAuth: PassportAuth) { }
  run() {
    const app = express();
    app.set("etag", false)
    app.use(cors({credentials: true, origin: true}))
    app.disable('x-powered-by');
    this.passportAuth.setup(app)

    const runtimeApi = createDefaultRuntimeApi(this.logger);
    if (runtimeApi) {
        this.logger.info(runtimeApi.controllers ? (runtimeApi.controllers.map(c => c.name).join(", ") || "[EMPTY]") : "NULL")
    }
    const builder = new CustomExpressRouteBuilder(this.logger, app, (cc, n) => kernel.get<any>(n));
    const ok = builder.build(runtimeApi);
    if (!ok) {
      this.logger.error("Unable to build express routes - Service stopping!")
      return false;
    }

    app.use(this.config.errorRequestHandler || errorRequestHandler)
    app.listen(this.config.port, () => {
      this.logger.info(`Cosmos API listening on port ${this.config.port}!`);
    });
    return true;
  }
}
