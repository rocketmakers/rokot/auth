import {IExpressApiRequest, ExpressRouteBuilder, ExpressApiRequest, IExpressRequest} from "rokot-apicontroller";
import {IProfile} from "../core";

export interface IRequest<TBody, TResponse, TParams, TQuery> extends IExpressApiRequest<TBody, TResponse, TParams, TQuery>{
  isAuthenticated(): boolean
  isUnauthenticated(): boolean
  user: IProfile
}

export interface IGetRequest<TResponse, TParams, TQuery> extends IRequest<void, TResponse, TParams, TQuery>{
}

export class CustomExpressApiRequest<TBody, TResponse, TParams, TQuery> extends ExpressApiRequest<TBody, TResponse, TParams, TQuery> implements IRequest<TBody, TResponse, TParams, TQuery>{
  user: IProfile
  constructor(native: IExpressRequest){
    super(native)
    this.user = native.request.user;
  }
  isAuthenticated(): boolean{
    return this.native.request.isAuthenticated()
  }
  isUnauthenticated(): boolean{
    return this.native.request.isUnauthenticated()
  }


  //doSomethingElse(){}
}

export class CustomExpressRouteBuilder extends ExpressRouteBuilder{
  protected createHandler(req: IExpressRequest){
    return new CustomExpressApiRequest<any, any, any, any>(req)
  }
}
