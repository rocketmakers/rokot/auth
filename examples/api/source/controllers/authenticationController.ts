import { api } from "rokot-apicontroller";
import * as express from "express";
import * as passport from "passport";
import { PostgresProfileRepository } from "../data/postgresProfileRepository";
import { IProfile, INewUserRequest } from "../core";
import { KnexConnection } from "../data/knexConnection";
import { InjectTypes } from "../ioc/injectTypes";

import {
  IUserCreationService,
  IUserVerificationService,
  IForgotPasswordService,
  IResetPasswordService,
  IChangePasswordService,
  ILoginService,
  IUserCreationResponse,
  IVerifyUserRequest,
  IForgotPasswordRequest,
  IForgotPasswordResponse,
  IResetPasswordRequest,
  IChangePasswordRequest,
  ILoginRequest
} from "../../../../rokot-auth";

import { IRequest, IGetRequest } from "../server/expressRequest";

// import {IExpressApiRequest} from "rokot-apicontroller";
// type IAppRequest<TBody, TResponse, TParams, TQuery> = IExpressApiRequest<TBody, TResponse, TParams, TQuery>
// type IAppGetRequest<TResponse, TParams, TQuery> = IExpressApiRequest<void, TResponse, TParams, TQuery>

class Middleware {
  @api.middlewareFunction("authenticate")
  static authenticate = passport.authenticate("local");

  @api.middlewareFunction("twitterLogin")
  static twitterLogin = passport.authenticate("twitter");

  @api.middlewareFunction("jwtAuthenticate")
  static jwtAuthenticate = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    passport.authenticate("jwt", { session: false }, (err: Error, user: any, info: any) => {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next();
      }
      if (user) {
        req.login(user, { session: false }, err => {
          if (err) {
            return next(err);
          }
          return next();
        })
      }
    })(req, res, next);
  };

  @api.middlewareFunction("protect")
  static protect = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (!req.isAuthenticated()) {
      res.status(401).send({ ok: false });
      return
    }

    next()
  };
}

@api.controller(InjectTypes.AuthenticationController, "auth")
export class AuthenticationController {
  constructor(
    private knexConnection: KnexConnection,
    private userCreationService: IUserCreationService,
    private userVerificationService: IUserVerificationService,
    private forgotPasswordService: IForgotPasswordService,
    private resetPasswordService: IResetPasswordService,
    private changePasswordService: IChangePasswordService,
    private profileRepository: PostgresProfileRepository
  ) { }

  @api.route("user")
  @api.verbs("post")
  async createUser(req: IRequest<INewUserRequest, IUserCreationResponse, void, void>) {
    await this.knexConnection.knex.transaction(async (t) => {
      const profileId = await this.profileRepository.createProfile(req.body.displayName, t);
      const result = await this.userCreationService.createUser(Object.assign({}, req.body, { profileId }), t);
      req.sendCreated(result);
    });
  }

  @api.route("verify")
  @api.verbs("post")
  async verifyUser(req: IRequest<IVerifyUserRequest, void, void, void>) {
    await this.knexConnection.knex.transaction(async (t) => {
      await this.userVerificationService.verifyUser(req.body, t);
      req.sendNoContent();
    });
  }

  @api.route("forgot-password")
  @api.verbs("post")
  async forgotPassword(req: IRequest<IForgotPasswordRequest, IForgotPasswordResponse, void, void>) {
    await this.knexConnection.knex.transaction(async (t) => {
      const result = await this.forgotPasswordService.forgotPassword(req.body, t);
      req.sendOk(result);
    });
  }

  @api.route("reset-password")
  @api.verbs("post")
  async resetPassword(req: IRequest<IResetPasswordRequest, void, void, void>) {
    await this.knexConnection.knex.transaction(async (t) => {
      await this.resetPasswordService.resetPassword(req.body, t);
      req.sendNoContent();
    });
  }

  @api.route("change-password")
  @api.verbs("post")
  async changePassword(req: IRequest<IChangePasswordRequest, void, void, void>) {
    await this.knexConnection.knex.transaction(async (t) => {
      await this.changePasswordService.changePassword(req.body, t);
      req.sendNoContent();
    });
  }

  @api.route("login")
  @api.verbs("post")
  @api.middleware("authenticate")
  async login(req: IRequest<ILoginRequest, void, void, void>) {
    req.sendNoContent();
  }

  @api.route("logout")
  @api.verbs("get")
  logout(req: IGetRequest<void, void, void>) {
    req.native.request.logout();
    req.sendNoContent();
  };

  @api.route("twitter")
  @api.verbs("get")
  @api.middleware("twitterLogin")
  async twitterLogin(req: IGetRequest<void, void, void>) {
    req.sendNoContent();
  }

  @api.route("twitter/callback")
  @api.verbs("get")
  @api.middleware("twitterLogin")
  async twitterCallback(req: IGetRequest<any, void, void>) {
    req.native.response.redirect("/auth/profiles/me");
  }

  //@api.middleware("jwtAuthenticate")
  @api.route("profiles/me")
  @api.verbs("get")
  @api.middleware("protect")
  async getProfile(req: IGetRequest<IProfile, void, void>) {
    req.sendOk(req.native.request.user);
  }
}
