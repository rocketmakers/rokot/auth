import { ISocialUser } from "../core";
import {KnexConnection} from "./knexConnection";

export class PostgresSocialLoginRepository {
  constructor(private connection: KnexConnection){}
  async getOrCreateSocialUser(profileId: number, provider: string, providerId: string, context?: any): Promise<ISocialUser> {
    const rows = await this.connection.knex("social_user").where({ provider, provider_id: providerId }).transacting(context as any);
    if (rows.length) {
      const row = rows[0];
      return {
        id: row["id"],
        provider: row["provider"],
        providerId: row["provider_id"],
        profileId: row["profile_id"]
      };
    }

    const id = await this.connection.knex("social_user").insert({ provider, provider_id: providerId, profile_id: profileId }).returning("id").transacting(context as any);
    return { id, provider, providerId, profileId };
  }
}
