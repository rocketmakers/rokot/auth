import { ITokenRepository, IDataContext, IVersionedHash } from "../../../../rokot-auth";
import {KnexConnection} from "./knexConnection";

export class PostgresTokenRepository implements ITokenRepository {
  constructor(private connection: KnexConnection){}
  async getVerificationTokenHashes(username: string, context: IDataContext): Promise<IVersionedHash[]> {
    const rows: any[] = await this.connection.knex("verification_token")
      .innerJoin("user", "user.id", "verification_token.user_id")
      .where({ "user.username": username })
      .select("verification_token.token_hash")
      .transacting(context as any);

    return rows.map(row => {
      const hashString = row["token_hash"] as string;
      const parts = hashString.split("~");
      return {
        version: +parts[0],
        hash: parts[1]
      };
    });
  }

  async createVerificationTokenHash(username: string, verificationTokenHash: IVersionedHash, context: IDataContext): Promise<void> {
    const hashString = `${verificationTokenHash.version}~${verificationTokenHash.hash}`;
    const idQuery = this.connection.knex("user").where({ username }).select("id");

    await this.connection.knex("verification_token").insert({ user_id: idQuery, token_hash: hashString }).transacting(context as any);
  }

  async destroyAllVerificationTokenHashes(username: string, context: IDataContext): Promise<void> {
    const idQuery = this.connection.knex("user").where({ username }).select("id");
    await this.connection.knex("verification_token")
      .where("user_id", "in", idQuery)
      .delete()
      .transacting(context as any);
  }

  async getPasswordResetTokenHashes(username: string, context: IDataContext): Promise<IVersionedHash[]> {
    const rows: any[] = await this.connection.knex("password_reset_token")
      .innerJoin("user", "user.id", "password_reset_token.user_id")
      .where({ "user.username": username })
      .select("password_reset_token.token_hash")
      .transacting(context as any);

    return rows.map(row => {
      const hashString = row["token_hash"] as string;
      const parts = hashString.split("~");
      return {
        version: +parts[0],
        hash: parts[1]
      };
    });
  }

  async createPasswordResetTokenHash(username: string, passwordResetTokenHash: IVersionedHash, context: IDataContext): Promise<void> {
    const hashString = `${passwordResetTokenHash.version}~${passwordResetTokenHash.hash}`;
    const idQuery = this.connection.knex("user").where({ username }).select("id");

    await this.connection.knex("password_reset_token").insert({ user_id: idQuery, token_hash: hashString }).transacting(context as any);
  }

  async destroyAllPasswordResetTokenHashes(username: string, context: IDataContext): Promise<void> {
    const idQuery = this.connection.knex("user").where({ username }).select("id");
    await this.connection.knex("password_reset_token")
      .where("user_id", "in", idQuery)
      .delete()
      .transacting(context as any);
  }
}
