import { IUser, INewUser, IUserRepository, IDataContext, IVersionedHash } from "../../../../rokot-auth";
import {KnexConnection} from "./knexConnection";

export class PostgresUserRepository implements IUserRepository {
  constructor(private connection: KnexConnection){}
  async userExists(username: string, context: IDataContext): Promise<boolean> {
    const count = await this.connection.knex("user").where({ username }).count().transacting(context as any);
    return count[0]["count"] > 0;
  }

  async getUserByUsername(username: string, context: IDataContext): Promise<IUser | null> {
    const rows = await this.connection.knex("user").where({ username }).transacting(context as any);
    if (!rows.length) {
      return null;
    }

    const row = rows[0];
    return {
      id: row["id"].toString(),
      username: row["username"],
      profileId: row["profile_id"]
    };
  }

  async createUser(newUser: INewUser, context: IDataContext): Promise<void> {
    await this.connection.knex("user").insert({ username: newUser.username, profile_id: newUser["profileId"] }).transacting(context as any);
  }

  async getPasswordHashByUsername(username: string, context: IDataContext): Promise<IVersionedHash> {
    const rows = await this.connection.knex("user").where({ username }).select("password_hash").transacting(context as any);
    const hashString = rows[0]["password_hash"] as string;
    const parts = hashString.split("~");
    return {
      version: Number(parts[0]),
      hash: parts[1]
    };
  }

  async setUserPassword(username: string, passwordHash: IVersionedHash, context: IDataContext): Promise<void> {
    const hashString = `${passwordHash.version}~${passwordHash.hash}`;
    await this.connection.knex("user").where({ username }).update({ password_hash: hashString }).transacting(context as any);
  }
}
