import { kernel, register } from "./kernel";
import { InjectTypes } from "./injectTypes";
import * as knexImport from "knex";
import { IApiConfig, Api } from "../server/api";
import { KnexConnection } from "../data/knexConnection";
import { Logger } from "rokot-log";
import { PassportAuth } from "../server/passport";

import {
  makeUserCreationService,
  makeUserVerificationService,
  makeForgotPasswordService,
  makeResetPasswordService,
  makeChangePasswordService,
  makeLoginService,
  IUserCreationService,
  IUserVerificationService,
  IForgotPasswordService,
  IResetPasswordService,
  IChangePasswordService,
  ILoginService,
  IUserRepository
} from "../../../../rokot-auth";

import { PostgresUserRepository } from "../data/postgresUserRepository";
import { PostgresTokenRepository } from "../data/postgresTokenRepository";
import { PostgresProfileRepository } from "../data/postgresProfileRepository";
import { PostgresSocialLoginRepository } from "../data/postgresSocialLoginRepository";

import { AuthenticationController } from "../controllers/authenticationController";
import { JwtController } from "../controllers/jwtController";

register(Api, InjectTypes.Api,
  InjectTypes.Logger,
  InjectTypes.ApiConfig,
  InjectTypes.PassportAuth)

register(KnexConnection, InjectTypes.KnexConnection,
  InjectTypes.Logger,
  InjectTypes.KnexConfig)

register(AuthenticationController, InjectTypes.AuthenticationController,
  InjectTypes.KnexConnection,
  InjectTypes.UserCreationService,
  InjectTypes.UserVerificationService,
  InjectTypes.ForgotPasswordService,
  InjectTypes.ResetPasswordService,
  InjectTypes.ChangePasswordService,
  InjectTypes.ProfileRepository
)

register(JwtController, InjectTypes.JwtController,
  InjectTypes.KnexConnection,
  InjectTypes.LoginService
)

register(PostgresUserRepository, InjectTypes.UserRepository,
  InjectTypes.KnexConnection)

register(PostgresSocialLoginRepository, InjectTypes.SocialLoginRepository,
  InjectTypes.KnexConnection)

register(PostgresTokenRepository, InjectTypes.TokenRepository,
  InjectTypes.KnexConnection)

register(PostgresProfileRepository, InjectTypes.ProfileRepository,
  InjectTypes.KnexConnection)

register(PassportAuth, InjectTypes.PassportAuth,
  InjectTypes.Logger,
  InjectTypes.KnexConnection,
  InjectTypes.ProfileRepository,
  InjectTypes.LoginService,
  InjectTypes.SocialLoginRepository)

export async function run(knexConfig: knexImport.Config, apiConfig: IApiConfig, logger: Logger) {
  logger.info("Binding")
  logger.info("-- Core Services")
  kernel.bind<Logger>(InjectTypes.Logger).toConstantValue(logger)

  logger.info("-- Configuration")
  kernel.bind<IApiConfig>(InjectTypes.ApiConfig).toConstantValue(apiConfig)
  kernel.bind<knexImport.Config>(InjectTypes.KnexConfig).toConstantValue(knexConfig)

  logger.info("-- Auth Services")
  const postgresUserRepository = kernel.get<PostgresUserRepository>(InjectTypes.UserRepository);
  const postgresTokenRepository = kernel.get<PostgresTokenRepository>(InjectTypes.TokenRepository);
  kernel.bind<IUserCreationService>(InjectTypes.UserCreationService).toConstantValue(makeUserCreationService(postgresUserRepository, postgresTokenRepository, logger));
  kernel.bind<IUserVerificationService>(InjectTypes.UserVerificationService).toConstantValue(makeUserVerificationService(postgresUserRepository, postgresTokenRepository, logger));
  kernel.bind<IForgotPasswordService>(InjectTypes.ForgotPasswordService).toConstantValue(makeForgotPasswordService(postgresUserRepository, postgresTokenRepository, logger));
  kernel.bind<IResetPasswordService>(InjectTypes.ResetPasswordService).toConstantValue(makeResetPasswordService(postgresUserRepository, postgresTokenRepository, logger));
  kernel.bind<IChangePasswordService>(InjectTypes.ChangePasswordService).toConstantValue(makeChangePasswordService(postgresUserRepository, postgresTokenRepository, logger));
  kernel.bind<ILoginService>(InjectTypes.LoginService).toConstantValue(makeLoginService(postgresUserRepository, logger));

  logger.info("Testing Connection")
  const connection = kernel.get<KnexConnection>(InjectTypes.KnexConnection);
  const ok = await connection.test()
  if (!ok) {
    logger.info("-- Connection Failed")
    connection.destroy()
    return false
  }
  logger.info("-- Passed")

  logger.info("Starting Api")
  const api = kernel.get<Api>(InjectTypes.Api)
  if (!api.run()) {
    logger.info("-- Failed")
    await connection.destroy()
    return false
  }
  logger.info("-- Started")
  return true;
}
