export class InjectTypes{
  static KnexConfig = "KnexConfig"
  static KnexConnection = "KnexConnection"

  static Logger = "Logger"

  static ApiConfig = "ApiConfig"
  static Api = "Api"

  static AuthenticationController = "AuthenticationController"
  static JwtController = "JwtController"


  static LoginService = "LoginService"
  static UserCreationService = "UserCreationService"
  static UserVerificationService = "UserVerificationService"
  static ForgotPasswordService = "ForgotPasswordService"
  static ResetPasswordService = "ResetPasswordService"
  static ChangePasswordService = "ChangePasswordService"
  static ProfileRepository = "ProfileRepository"
  static SocialLoginRepository = "SocialLoginRepository"
  static UserRepository = "UserRepository"
  static TokenRepository = "TokenRepository"

  static PassportAuth = "PassportAuth"
}
