var fs = require("fs");
var path = require("path");
var glob = require('glob');
var webpack = require("webpack");
var sourcePath = path.join(__dirname, "source");
var tsConfigPath = path.join(sourcePath, "tsconfig.json");
var entryFile = path.join(sourcePath, "index.ts");
var generatorFile = path.join(sourcePath, "clientGenerator.ts");
var outputPath = path.join(__dirname, "dist");
var failPlugin = require("webpack-fail-plugin");

var node_modules = fs.readdirSync(path.join(__dirname, "node_modules")).filter(function(x) { return x !== '.bin' });
var tests = glob.sync(path.join(sourcePath, "test/**/*.ts"));

module.exports = {
  resolve: {
    extensions: ["", ".ts", ".js"]
  },
  devtool: "eval",
  target: "node",
  entry: {
    main: entryFile,
    generator: generatorFile
  },
  output: {
    path: outputPath,
    filename: "[name].js",
    libraryTarget: "commonjs"
  },
  externals: node_modules,
  module: {
    loaders: [{
      test: /\.ts$/,
      loader: "ts-loader?tsconfig=" + tsConfigPath
    }]
  },
  plugins: [
    failPlugin
  ]
};
