CREATE TABLE social_user (
  id serial PRIMARY KEY,
  profile_id int REFERENCES profile (id),
  provider text NOT NULL,
  provider_id text NOT NULL
);
