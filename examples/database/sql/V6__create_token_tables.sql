CREATE TABLE password_reset_token (
  id serial PRIMARY KEY,
  user_id int REFERENCES "user" (id),
  token_hash text NOT NULL
);

CREATE TABLE verification_token (
  id serial PRIMARY KEY,
  user_id int REFERENCES "user" (id),
  token_hash text NOT NULL
);

ALTER TABLE "user" DROP COLUMN verification_token_hash;
ALTER TABLE "user" DROP COLUMN password_reset_token_hash;
